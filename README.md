# Distributed tracing system zipkin

## zipkin简介
Zipkin 是一款开源的分布式实时数据追踪系统（Distributed Tracking System），基于 Google Dapper 的论文设计而来，由 Twitter 公司开发贡献。其主要功能是聚集来自各个异构系统的实时监控数据，用来追踪微服务架构下的系统延时问题。

应用系统需要进行装备（instrument）以向 Zipkin 报告数据。Zipkin 的用户界面可以呈现一幅关联图表，以显示有多少被追踪的请求通过了每一层应用。

Zipkin 主要由四部分构成：收集器、数据存储、查询以及 Web 界面。Zipkin 的收集器负责将各系统报告过来的追踪数据进行接收；而数据存储默认使用 Cassandra，也可以替换为 MySQL；查询服务用来向其他服务提供数据查询的能力，而 Web 服务是官方默认提供的一个图形用户界面。

## Brave简介

Brave 是用来装备 Java 程序的类库，提供了面向 Standard Servlet、Spring MVC、Http Client、JAX RS、Jersey、Resteasy 和 MySQL 等接口的装备能力，可以通过编写简单的配置和代码，让基于这些框架构建的应用可以向 Zipkin 报告数据。


##方案选型
现有可选方案是

>twitter的zipkin
>
>阿里的“鹰眼”
>
>京东的“Hydra”
>
>窝窝的"Tracing"

其中，开源并且持续更新的只有zipkin。为了能够减小实现的成本，我们需要尽可能的使用稳定的开源资源，所以我们会在zipkin的基础上实现跟踪功能。


##服务器部署方案
推荐在zipkin服务端使用docker部署zipkin，数据库选型使用Elasticsearch集群

##客户端集成方案

### zipkin 安装
安装路径：
>git clone https://github.com/openzipkin/zipkin-js.git

启动命令：
>java -jar ./zipkin-server/target/zipkin-server-*exec.jar

使用mysql数据库的启动命令：
>STORAGE_TYPE=mysql MYSQL_USER=root java -jar ./zipkin-server/target/zipkin-server-*exec.jar

### Java实现的service

对于使用java实现的service，可以利用brave框架实现对该service的消息跟踪。这里使用的是servlet拦截器.

ZipkinConfig

```
@Configuration
public class ZipkinConfig {

    @Bean
    public SpanCollector spanCollector() {
        HttpSpanCollector.Config spanConfig = HttpSpanCollector.Config.builder()
                .compressionEnabled(false)//默认false，span在transport之前是否会被gzipped。
                .connectTimeout(5000)//5s，默认10s
                .flushInterval(1)//1s
                .readTimeout(6000)//5s，默认60s
                .build();
        return HttpSpanCollector.create("http://localhost:9411",
                spanConfig,
                new EmptySpanCollectorMetricsHandler());
    }

    @Bean
    public Brave brave(SpanCollector spanCollector) {
        Brave.Builder builder = new Brave.Builder("service1");//指定serviceName
        builder.spanCollector(spanCollector);
        builder.traceSampler(Sampler.create(1));//采集率
        return builder.build();
    }

    @Bean
    public BraveServletFilter braveServletFilter(Brave brave) {
        /**
         * 设置sr、ss拦截器
         */
        return new BraveServletFilter(brave.serverRequestInterceptor(),
                brave.serverResponseInterceptor(),
                new DefaultSpanNameProvider());
    }

    @Bean
    public OkHttpClient okHttpClient(Brave brave){
        /**
         * 设置cs、cr拦截器
         */
        return new OkHttpClient.Builder()
                .addInterceptor(new BraveOkHttpRequestResponseInterceptor(brave.clientRequestInterceptor(),
                        brave.clientResponseInterceptor(),
                        new DefaultSpanNameProvider()))
                .build();
    }
}

```

ZipkinBraveController

```
@Api("zipkin brave api")
@RestController
@RequestMapping("/zipkin/brave/service1")

public class ZipkinBraveController {

    @Autowired
    private OkHttpClient okHttpClient;

    @ApiOperation("trace第一步")
    @RequestMapping("/test1")
    public String myboot() throws Exception {
        Thread.sleep(100);//100ms
        Request request = new Request.Builder().url("http://localhost:8032/zipkin/brave/service2/test2").build();
        /*
         * 1、执行execute()的前后，会执行相应的拦截器（cs,cr）
         * 2、请求在被调用方执行的前后，也会执行相应的拦截器（sr,ss）
         */
        Response response = okHttpClient.newCall(request).execute();
        return response.body().string();
    }
}

```

### NodeJs实现的service
NodeJs实现的service如果使用的是express框架，可以参考如下代码

```

var express = require('express');
var path = require('path');
const {HttpLogger}= require('zipkin-transport-http')

//New for Zipkin
const {Tracer, ExplicitContext, BatchRecorder, ConsoleRecorder} = require('zipkin');

var zipkinMiddleware = require('zipkin-instrumentation-express').expressMiddleware;
const {ScribeLogger} = require('zipkin-transport-scribe');
const CLSContext = require('zipkin-context-cls');


var ctxImpl = new ExplicitContext();
var recorder = new BatchRecorder({
    logger: new HttpLogger({
        endpoint: 'http://localhost:9411/api/v1/spans'
    })
});


const tracer = new Tracer({
recorder: recorder,
ctxImpl: new CLSContext('zipkin')
});


var app = express();

//Add the Zipkin Middleware
app.use(zipkinMiddleware({
  tracer,
  serviceName: 'zipkinTestingParent'
}));

app.get('/', function(req, res){
	res.send('Hello World');
})

var server = app.listen(8081, function () {
 
  var host = server.address().address
  var port = server.address().port
 
  console.log("应用实例，访问地址为 http://%s:%s", host, port)
 
})

```
