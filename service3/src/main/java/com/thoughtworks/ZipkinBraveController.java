package com.thoughtworks;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("zipkin brave api")
@RestController
@RequestMapping("/zipkin/brave/service3")
public class ZipkinBraveController {

    @ApiOperation("trace第三步")
    @RequestMapping(value="/test3",method=RequestMethod.GET)
    public String myboot() throws Exception {
        Thread.sleep(300);//100ms
        return "service3";
    }

}