package com.thoughtworks;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("zipkin brave api")
@RestController
@RequestMapping("/zipkin/brave/service4")
public class ZipkinBraveController {

    @ApiOperation("trace第4步")
    @RequestMapping(value = "/test4", method = RequestMethod.GET)
    public String myboot() throws Exception {
        Thread.sleep(400);//100ms
        return "service4";
    }

}