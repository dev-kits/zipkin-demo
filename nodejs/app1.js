var express = require('express');
var path = require('path');
const {HttpLogger}= require('zipkin-transport-http')

//New for Zipkin
const {Tracer, ExplicitContext, BatchRecorder, ConsoleRecorder} = require('zipkin');

var zipkinMiddleware = require('zipkin-instrumentation-express').expressMiddleware;
const {ScribeLogger} = require('zipkin-transport-scribe');
const CLSContext = require('zipkin-context-cls');


var ctxImpl = new ExplicitContext();
var recorder = new BatchRecorder({
    logger: new HttpLogger({
        endpoint: 'http://localhost:9411/api/v1/spans'
    })
});


const tracer = new Tracer({
recorder: recorder,
ctxImpl: new CLSContext('zipkin')
});


var app = express();

//Add the Zipkin Middleware
app.use(zipkinMiddleware({
  tracer,
  serviceName: 'zipkinTestingParent'
}));

app.get('/', function(req, res){
	res.send('Hello World');
})

var server = app.listen(8081, function () {
 
  var host = server.address().address
  var port = server.address().port
 
  console.log("应用实例，访问地址为 http://%s:%s", host, port)
 
})
