package com.thoughtworks;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@Api("zipkin brave api")
@RestController
@RequestMapping("/zipkin/brave/service2")
public class ZipkinBraveController {

    @Autowired
    private OkHttpClient okHttpClient;

    @ApiOperation("trace第二步")
    @RequestMapping(value="/test2",method=RequestMethod.GET)
    public String myboot() throws Exception {
        Thread.sleep(200);//100ms
        Request request3 = new Request.Builder().url("http://localhost:8033/zipkin/brave/service3/test3").build();
        Response response3 = okHttpClient.newCall(request3).execute();
        String response3Str = response3.body().string();

        Request request4 = new Request.Builder().url("http://localhost:8034/zipkin/brave/service4/test4").build();
        Response response4 = okHttpClient.newCall(request4).execute();
        String response4Str = response4.body().string();

        return response3Str + "-" +response4Str;
    }

}